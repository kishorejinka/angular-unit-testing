import { HeroComponent } from './hero/hero.component';
  
export const HEROES :HeroComponent[] = [
    { id: 11, name: 'Thor' },
    { id: 12, name: 'Pepper' },
    { id: 13, name: 'Iron Man' },
    { id: 14, name: 'Hulk' },
    { id: 15, name: 'Superman' },
    { id: 16, name: 'Ant Man' },
    { id: 17, name: 'Spiderman' },
    { id: 18, name: 'BatMan' },
    { id: 19, name: 'XMen' },
    { id: 20, name: 'Loki' }
]; 