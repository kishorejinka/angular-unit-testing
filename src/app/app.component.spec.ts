/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';


// describe('Users factory', function() {
//   it('has a dummy spec to test 2 + 2', function() {
//     // An intentionally failing test. No code within expect() will never equal 4.
//     expect(4).toEqual(4);
//   });
// });
describe('AppComponent', () => {
    // beforeEach(() => {
  //   TestBed.configureTestingModule({
  //     declarations: [
  //       AppComponent
  //     ],
  //   });
  //   TestBed.compileComponents();
  // });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it(`should have as title 'app works!'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app works123!');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('app works123!');
  }));
});


// describe('Sanity Test', () => {

//   it('Should test matchers', () => {

//       let _undefined, _defined = true;
      
//       expect("a" + "b").toBe("ab");

//       expect(_undefined).toBeUndefined();
//       expect(_defined).toBeDefined();

//       expect(!_defined).toBeFalsy();
//       expect(_defined).toBeTruthy();
//       expect(null).toBeNull();

//       expect(1 + 1).toEqual(2);
//       expect(5).toBeGreaterThan(4);
//     expect(5).toBeLessThan(6);

//       expect("abcdbca").toContain("bcd");
//       expect([4, 5, 6]).toContain(5);
//       expect("abcdefgh").toMatch(/efg/);

//       expect("abcdbca").not.toContain("xyz");
//       expect("abcdefgh").not.toMatch(/123/);
//   });
// });