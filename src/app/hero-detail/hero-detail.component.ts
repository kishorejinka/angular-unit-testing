import { HeroComponent } from './../hero/hero.component';
import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core/src/metadata/directives';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
  @Input() hero: HeroComponent;
  constructor() { }

  ngOnInit() {
    console.log(this.hero)
  }

}
