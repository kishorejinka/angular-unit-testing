import { HEROES } from './../mock-heroes';
import { HeroComponent } from './../hero/hero.component';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  hero: HeroComponent = {
    id: 1,
    name: "Tom Cruise"
  };

 heroes =  HEROES;
 selectedHero: HeroComponent;

  constructor() { }

  ngOnInit() {
  }
  onSelect(hero : HeroComponent):void{
this.selectedHero = hero;
  }
}
