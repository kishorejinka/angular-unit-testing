import { HeroComponent } from './hero/hero.component';
import { Injectable } from '@angular/core';
import { HEROES } from './mock-heroes';

@Injectable()
export class HeroService {

  constructor() { }
  getHeroes(): HeroComponent[] {
    return HEROES;
  }
}
