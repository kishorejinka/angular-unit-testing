// // Turn on full stack traces in errors to help debugging
// // Error.stackTraceLimit = Infinity;
// Error.stackTraceLimit = 0;


// jasmine.DEFAULT_TIMEOUT_INTERVAL = 3000;

// // // Cancel Karma's synchronous start,
// // // we will call `__karma__.start()` later, once all the specs are loaded.
// __karma__.loaded = function() {};


// System.config({
//   packages: {
//     'base/app': {
//       defaultExtension: false,
//       format: 'register',
//       map: Object.keys(window.__karma__.files).
//             filter(onlyAppFiles).
//             reduce(function createPathRecords(pathsMapping, appPath) {
//               var moduleName = appPath.replace(/^\/base\/app\//, './').replace(/\.js$/, '');
//               pathsMapping[moduleName] = appPath + '?' + window.__karma__.files[appPath]
//               return pathsMapping;
//             }, {})

//       }
//     }
// });

// System.import('angular2/testing').then(function(testing) {
//   return System.import('angular2/platform/testing/browser').then(function(providers) {
//     testing.setBaseTestProviders(providers.TEST_BROWSER_PLATFORM_PROVIDERS,
//                                  providers.TEST_BROWSER_APPLICATION_PROVIDERS);
//   });
// }).then(function() {
//   return Promise.all(
//     Object.keys(window.__karma__.files) // All files served by Karma.
//     .filter(onlySpecFiles)
//     // .map(filePath2moduleName)        // Normalize paths to module names.
//     .map(function(moduleName) {
//       // loads all spec files via their global module names (e.g. 'base/public/app/hero.service.spec')
//       return System.import(moduleName);
//     }));
// })
// .then(function() {
//   __karma__.start();
// }, function(error) {
//   __karma__.error(error.stack || error);
// });


// function filePath2moduleName(filePath) {
//   return filePath.
//            replace(/^\//, '').              // remove / prefix
//            replace(/\.\w+$/, '');           // remove suffix
// }


// function onlyAppFiles(filePath) {
//   return /^\/base\/app\/.*\.js$/.test(filePath)
// }


// function onlySpecFiles(path) {
//   return /^\/base\/test\/.*\.js$/.test(path);
// }



Error.stackTraceLimit = 0;
jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000;

// (1)
var builtPaths = (__karma__.config.builtPaths || ['app/'])
                  .map(function(p) { return '/base/'+p;});

// (2)
__karma__.loaded = function () { };

// (3)
function isJsFile(path) {
  return path.slice(-3) == '.js';
}

// (4)
function isSpecFile(path) {
  return /\.spec\.(.*\.)?js$/.test(path);
}

// (5)
function isBuiltFile(path) {
  return isJsFile(path) &&
          builtPaths.reduce(function(keep, bp) {
            return keep || (path.substr(0, bp.length) === bp);
          }, false);
}

// (6)
var allSpecFiles = Object.keys(window.__karma__.files)
  .filter(isSpecFile)
  .filter(isBuiltFile);

// (7)
SystemJS.config({
  baseURL: 'base'
});

// (8)
System.import('systemjs.config.js')
  .then(initTesting);

// (9)
function initTesting () {
  return Promise.all(
    allSpecFiles.map(function (moduleName) {
      return System.import(moduleName);
    })
  )
  .then(__karma__.start, __karma__.error);
}